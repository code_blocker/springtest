package com.practise.config;

import com.practise.model.BigBox;
import com.practise.model.HelloWorld;
import com.practise.model.Profile;
import com.practise.model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
    public static void main(String[] args) {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(BigSmallBoxConfig.class);
        BigBox obj = context.getBean(BigBox.class);
        HelloWorld obj1 = context.getBean(HelloWorld.class);
        obj.checkBigBox();
        obj1.setMessage("Hello World");
        obj1.getMessage();
        context.registerShutdownHook();
    }
}
