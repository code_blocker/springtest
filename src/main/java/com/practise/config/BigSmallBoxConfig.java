package com.practise.config;

import com.practise.model.BigBox;
import com.practise.model.SmallBox;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(HelloWorldConfig.class)
public class BigSmallBoxConfig {

    @Bean
    public BigBox bigBox(){
        return new BigBox();
    }

    @Bean
    public SmallBox smallBox(){
        return new SmallBox();
    }
}
