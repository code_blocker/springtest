package com.practise.model;

import org.springframework.beans.factory.annotation.Autowired;

public class BigBox {

    @Autowired
    private SmallBox smallBox;

    public BigBox() {
        System.out.println("Big Box Constructor");
    }

    public SmallBox getSmallBox() {
        return smallBox;
    }

    public void checkBigBox(){
        smallBox.checkSmallBox();
    }
}
